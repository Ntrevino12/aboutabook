"use client";
import { Inter } from "next/font/google";
import "./globals.css";
import { AuthProvider } from "@galvanize-inc/jwtdown-for-react";
import NavControl from "../components/NavControl"
import React from "react";

const inter = Inter({ subsets: ["latin"] });

export default function RootLayout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {
  // Cant Get var since its not put into file at build time


  return (
    <html lang="en">
      <body className={inter.className}>
        <AuthProvider baseUrl={"http://localhost:8000"}>
          <title>AboutABook</title>
          <NavControl/>
          {children}
        </AuthProvider>
      </body>
    </html>
  );
}
