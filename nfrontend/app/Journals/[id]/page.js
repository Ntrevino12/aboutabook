"use client";
import { useState, useEffect, useContext } from "react";
import { AuthContext } from "@galvanize-inc/jwtdown-for-react";
import { useParams } from "next/navigation";
import JournalList from "../../../components/JournalList";
import useUser from "../../../components/UseUser";
import { CgAddR } from "react-icons/cg";
import Popup from "../../../components/Popup";
import JournalForm from "../../../components/Journalform";

export default function Journal() {
  const book = useParams();
  const [journals, setJournals] = useState([]);
  const { token } = useContext(AuthContext);
  const [editFormJ, setEditFormJ] = useState(false);
  const [journal, setjournal] = useState(null);
  const [createFormJ, setCreateFormJ] = useState(false);
  const user = useUser(token);


      // opens window for editting a book
  const editJournalToggle = (journal) => {
    setEditFormJ(!editFormJ);
    setjournal(journal);
  };

  const getJournals = async () => {
    const journalListUrl = `http://localhost:8000/api/books/${book.id}/journals`;
    const config = {
      method: "get",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
      },
    };
    const response = await fetch(journalListUrl, config);

    if (response.ok) {
      const data = await response.json();
      setJournals(data);
    }
  };

  useEffect(() => {
    getJournals();
    // eslint-disable-next-line
  }, []);

  return (
    <div className="pt-[110px]">
      <div className="flex justify-end mx-6">
        <div className="relative">
          <button
            className="text-white hover:text-[#d2a166] bg-[#402A1E] hover:bg-[#4f3425] py-3 px-3 rounded-full"
            onClick={() => setCreateFormJ(true)}
          >
            <CgAddR />
          </button>
        </div>
      </div>
      <Popup trigger={createFormJ}>
        <JournalForm
          book_id={book.id}
          setCreateFormJ={setCreateFormJ}
          token={token}
          user={user}
          getJournals={getJournals}
        />
      </Popup>
      <JournalList
        book_id={book.id}
        journal={journal}
        editFormJ={editFormJ}
        setEditFormJ={setEditFormJ}
        getJournals={getJournals}
        journals={journals}
        token={token}
        JournalForm={JournalForm}
        editJournalToggle={editJournalToggle}
      />
    </div>
  );
}
