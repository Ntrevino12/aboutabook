'use client'
import { useState, useEffect, useContext } from "react";
import { AuthContext } from "@galvanize-inc/jwtdown-for-react";
import { CgAddR } from "react-icons/cg";
import ListBooks from "../../components/ListBooks";
import BookForm from "../../components/BookForm";
import Popup from "../../components/Popup";
import useUser from "../../components/UseUser";

export default function Books(){
    const [books, setBooks] = useState([]);
    const { token } = useContext(AuthContext);
    const [editFormB, setEditFormB] = useState(false);
    const [book, setbook] = useState(null);
    const [createFormT, setCreateFormT] = useState(false);
    const user = useUser(token);

    // opens window for editting a book
    const editBook = (book) => {
      setEditFormB(!editFormB);
      setbook(book);
    };

    // get list of books
    const getBooks = async () => {
      const bookListUrl = `http://localhost:8000/api/books`;
      const config = {
        method: "get",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${token}`,
        },
      };
      const response = await fetch(bookListUrl, config);

      if (response.ok) {
        const data = await response.json();
        setBooks(data);
      }
    };

    useEffect(() => {
      getBooks();
      // eslint-disable-next-line
    }, []);

    return (
      <div className="pt-[110px]">
        <div className="flex justify-end mx-6">
          <div className="relative">
            <button
              className="text-white hover:text-[#d2a166] bg-[#402A1E] hover:bg-[#4f3425] py-3 px-3 rounded-full"
              onClick={() => setCreateFormT(true)}
            >
              <CgAddR />
            </button>
          </div>
        </div>
        <Popup trigger={createFormT}>
          <BookForm
            setCreateFormT={setCreateFormT}
            token={token}
            user={user}
            getBooks={getBooks}
          />
        </Popup>
        <ListBooks
          book={book}
          editFormB={editFormB}
          setEditFormB={setEditFormB}
          getBooks={getBooks}
          books={books}
          token={token}
          BookForm={BookForm}
          editBook={editBook}
        />
      </div>
    );
}
