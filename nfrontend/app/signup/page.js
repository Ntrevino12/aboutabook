"use client";
import { useState } from "react";
import { useRouter } from "next/navigation";
import useToken from "@galvanize-inc/jwtdown-for-react";
import book from "../../public/Images/signupbook.jpg";
import Image from "next/image";

export default function SignUp() {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const { login } = useToken();
  const router = useRouter();
  const [email, setEmail] = useState("");

  const handleSubmit = async (e) => {
    e.prevent.default();
    const data = {};
    data.username = username;
    data.password = password;
    data.email = email;

    const url = `http://localhost:8000/api/account`;
    const fetchcongif = {
      method: "POST",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(url, fetchcongif);

    if (response.ok) {
      await response.json();
      try {
        await login(username, password);
        setUsername("");
        setPassword("");
        setEmail("");
        router.push("/books");
      } catch (error) {
        console.error("Cant log in:", error);
      }
    } else {
      console.error("Failed to create account");
    }
  };
  return (
    <>
      <div className="h-screen text-center pt-[10%]">
        <div className="h-full md:w-[80%] lg:w-[55%] xl:w-[50%] 2xl:w-[35%] pt-[200px] m-auto">
          <div className="border-solid border-4 border-slate-900 shadow-xl shadow-gray-600 rounded-xl p-8 2xl:p-10">
            <form onSubmit={handleSubmit}>
              <div className="xl:h-[60%] 2xl:h-[40%] grid grid-row-6 grid-flow-col gap-7">
                <div className="row-span-6">
                  <h1>SignUp For AboutABook</h1>
                  <div className="flex justify-center pt-5">
                    <Image
                      className="border-solid border-4 border-slate-900 rounded-lg md:w-[80%] lg:w-[90%] xl:w-[80%] 2xl:w-[60%]"
                      src={book}
                      loading="lazy"
                      alt=""
                    />
                  </div>
                </div>
                <div className="row-start-2 row-end-4">
                  <div className="form-floating">
                    <p>
                      <label htmlFor="username">Username</label>
                    </p>
                    <input
                      type="text"
                      className="form-control border-2 rounded-lg p-2 w-[250px] border-gray-300"
                      id="username"
                      value={username}
                      onChange={(e) => setUsername(e.target.value)}
                      required
                      placeholder=""
                    />
                  </div>
                  <div className="form-floating">
                    <p>
                      <label htmlFor="password">Password</label>
                    </p>
                    <input
                      type="text"
                      className="form-control border-2 rounded-lg p-2 w-[250px] border-gray-300"
                      id="password"
                      value={password}
                      onChange={(e) => setPassword(e.target.value)}
                      required
                      placeholder=""
                    />
                  </div>
                  <div className="form-floating">
                    <p>
                      <label htmlFor="email">Email</label>
                    </p>
                    <input
                      type="text"
                      className="form-control border-2 rounded-lg p-2 w-[250px] border-gray-300"
                      id="email"
                      value={email}
                      onChange={(e) => setEmail(e.target.value)}
                      placeholder=""
                    />
                  </div>
                </div>
                <button className="row-start-5 row-end-6 py-3 px-4 rounded-lg shadow-md text-white bg-[#402A1E]">
                  Submit
                </button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </>
  );
}
