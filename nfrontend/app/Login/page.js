'use client'
import { useState, useEffect, useContext } from "react";
import useToken, { AuthContext } from "@galvanize-inc/jwtdown-for-react";
import useUser from "../../components/UseUser"
import book from "../../public/Images/loginbook.jpg";
import { useRouter } from "next/navigation";
import Image from "next/image";

export default function Login(){
    const { login } = useToken();
    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");
    const router = useRouter();
    const { token } = useContext(AuthContext);
    const user = useUser(token);

    const handleSubmit = async (e) => {
      e.preventDefault();
      try {
        await login(username, password);
        setUsername("");
        setPassword("");
      } catch (error) {
        console.error("cant log in", error);
      }
    };

    useEffect(() => {
      if (user) {
        router.push("/books");
      }
    });
    // need to fix form breaks at 1504x1162
    return (
      <>
        <div className="h-screen w-full text-center pt-[10%]">
          <div className="relative h-full md:w-[80%] lg:w-[55%] xl:w-[50%] 2xl:w-[35%] pt-[200px] m-auto">
            <div className="border-solid border-4 border-slate-900 shadow-xl shadow-gray-600 rounded-xl p-8 2xl:p-10">
              <form onSubmit={(e) => handleSubmit(e)}>
                <div className=" xl:h-[60%] 2xl:h-[40%] grid grid-row-6 grid-flow-col gap-7">
                  <div className="row-span-6">
                    <h1>Log in For AboutABook</h1>
                    <div className="flex justify-center pt-5">
                      <Image
                        className="border-solid border-4 border-slate-900 rounded-lg md:w-[80%] lg:w-[90%] xl:w-[80%] 2xl:w-[55%]"
                        src={book}
                        loading="lazy"
                        alt=""
                      />
                    </div>
                  </div>
                  <div className="row-start-2 row-end-4">
                    <div className="form-floating">
                      <p>
                        <label htmlFor="username">Username</label>
                      </p>
                      <input
                        type="text"
                        className="form-control border-2 rounded-lg p-2 w-[250px] border-gray-300"
                        id="username"
                        value={username}
                        onChange={(e) => setUsername(e.target.value)}
                        required
                        placeholder=""
                      />
                    </div>
                    <div className="form-floating">
                      <p>
                        <label htmlFor="password">Password</label>
                      </p>
                      <input
                        type="password"
                        className="form-control border-2 rounded-lg p-2 w-[250px] border-gray-300"
                        id="password"
                        value={password}
                        onChange={(e) => setPassword(e.target.value)}
                        required
                        placeholder=""
                      />
                    </div>
                  </div>
                  <button className="row-start-5 row-end-6 py-3 px-4 rounded-lg shadow-md text-white bg-[#402A1E]">
                    Login
                  </button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </>
    );
}
