"use client";
import HomePageCarousel from "../components/HomePageCarousel";
import { useContext } from "react";
import { AuthContext } from "@galvanize-inc/jwtdown-for-react";
import useUser from "../components/UseUser";

export default function Home() {
  const { token } = useContext(AuthContext);
  const user = useUser(token);
  return (
    <main className="h-fit">
      <div className="pt-52">
        <div className="relative z-20 flex items-center bg-[#402A1E]">
          <div className="container relative flex flex-col items-center justify-between px-6 py-8 mx-auto">
            <div className="flex flex-col">
              <h1 className="w-full text-4xl font-light text-center text-gray-800 uppercase sm:text-5xl dark:text-white">
                Welcome to AboutABook
              </h1>
              <h2 className="w-full max-w-2xl py-8 mx-auto text-xl font-light text-center text-gray-500 dark:text-white">
                Welcome to AboutABook, your digital haven for crafting and
                organizing your literary adventures. Whether you&apos;re an avid
                reader, a dedicated reviewer, or a passionate writer, our
                application is designed to be your personalized space for
                curating and sharing your book journals. With seamless features
                tailored to your reading preferences, AboutABook is here to
                transform the way you engage with literature, one page at a
                time.
              </h2>
              <div
                className={`flex items-center justify-center mt-4 pb-4 ${
                  !user ? "block" : "hidden"
                }`}
              >
                <a
                  href="/Login"
                  className="px-4 py-2 mr-4 text-white uppercase bg-gray-800 border-2 border-transparent text-md hover:bg-gray-900"
                >
                  Login
                </a>
                <a
                  href="/signup"
                  className="px-4 py-2 text-gray-800 uppercase bg-transparent border-2 border-gray-800 dark:text-white hover:bg-gray-800 hover:text-white text-md"
                >
                  Signup
                </a>
              </div>
            </div>
            <div className="relative block w-full mx-auto mt-6 md:mt-0">
              <HomePageCarousel />
            </div>
          </div>
        </div>
      </div>
    </main>
  );
}
