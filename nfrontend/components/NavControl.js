'use client'
import {useState, useEffect} from "react"
import Nav from "./Nav.js"
import SideNav from "./SideNav.js";

export default function NavControl() {
    const [width, setWidth] = useState(0)
    const breakpoint = 900;

    //windows.innerWidth making refernce error
    useEffect(() => {
        const handleWindowResize = () =>setWidth(window.innerWidth)
        window.addEventListener("resize", handleWindowResize);

        return () =>window.removeEventListener("resize", handleWindowResize);
    }, []);

    return width < breakpoint ? <SideNav /> : <Nav />
}
