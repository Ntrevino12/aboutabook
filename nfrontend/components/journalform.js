"use client";
import { useState } from "react";
import { FaTimes } from "react-icons/fa";
import createbook from "../public/Images//createbook.jpg";
import Image from "next/image";

function JournalForm(props) {
  const [summary, setSummary] = useState("");

  const handleSummaryChange = (e) => {
    const value = e.target.value;
    setSummary(value);
  };

  // Create journal for specific book
  const handleSubmit = async (e) => {
    e.preventDefault();
    const data = {};
    data.user_id = props.user.id;
    data.book_id = props.book_id;
    data.summary = summary;


    const journalUrl = `http://localhost:8000/api/books/${props.book_id}/journals`;
    const fetchConfig = {
      method: "POST",
      body: JSON.stringify(data),
      headers: {
        Authorization: `Bearer ${props.token}`,
        "Content-Type": "application/json",
      },
    };

    const response = await fetch(journalUrl, fetchConfig);

    if (response.ok) {
      await response.json();
      setSummary("");
      props.getJournals();
      props.setCreateFormJ(false)
    } else {
      console.error("Could not add journal, please try again");
    }
  };

  return (
    <>
      <div className="text-center">
        <div className="flex justify-end">
          <button
            className="close-btn p-3 rounded-lg text-white bg-[#402A1E] "
            onClick={() => props.setCreateFormJ(false)}
          >
            <FaTimes />
          </button>
        </div>
        <div className="flex justify-center">
          <Image
            className="w-[65%] h-[220px]  shadow-lg border-4 rounded-full"
            src={createbook}
            loading="lazy"
            alt=""
          />
        </div>
        <h1 className="pt-[5%]">Add to your Journals</h1>
        <div className="max-w-[500px] pt-[90px] m-auto px-2 py-16 w-full">
          <div className="col-span-3 h-auto shadow-xl shadow-gray-900 rounded-xl lg:p-4">
            <div className="py-4">
              <form onSubmit={handleSubmit}>
                <div className="grid lg:grid-cols-1 gap-4 py-2">
                  <div className="form-floating">
                    <p>
                      <label htmlFor="summary">Summary</label>
                    </p>
                    <input
                      type="text"
                      className="form-control border-2 rounded-lg p-2 w-[250px] border-gray-300"
                      id="summary"
                      value={summary}
                      onChange={handleSummaryChange}
                      required
                      placeholder=""
                    />
                  </div>
                </div>
                <div className="pt-[5%]">
                  <button className="py-3 px-4 rounded-lg shadow-md text-white bg-[#402A1E]">
                    ADD
                  </button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export default JournalForm;
