"use client";
import JournalEdit from "./JournalEdit";
import Popup from "./Popup";
import JournalMenu from "./JournalMenu";

export default function JournalList(props) {
  // Delete journals by journal id
  const handleDelete = async (journal_id) => {
    const deleteUrl = `http://localhost:8000/api/journals/${journal_id}`;
    const config = {
      method: "delete",
      headers: { Authorization: `Bearer ${props.token}` },
    };
    const response = await fetch(deleteUrl, config);
    if (response.ok) {
      await props.getJournals(props.book_id);
    }
  };

  return (
    <div>
      {props.journals ? (
        <div>
          <div className="pt-2">
            <ul className="grid gap-[1%] md:grid-cols-2 lg:grid-cols-3 xl:grid-cols-3 2xl:grid-cols-6">
              {props.journals.map((item) => {
                return (
                  <li
                    key={item.id}
                    className="max-w-lg rounded overflow-hidden shadow-lg border-4 border-[#402A1E]"
                  >
                    <div className="flex justify-end">
                      <JournalMenu
                        handleDelete={handleDelete}
                        editJournalToggle={props.editJournalToggle}
                        item={item}
                      />
                    </div>
                    <div className="px-4 py-3">
                      <div className="font-bold text-xl mb-2">
                        {item.summary}
                      </div>
                      <p className="text-gray-700 text-base">{item.book_id}</p>
                    </div>
                  </li>
                );
              })}
            </ul>
            <div>
              <Popup trigger={props.editFormJ}>
                <JournalEdit
                  token={props.token}
                  journal={props.journal}
                  setEditFormJ={props.setEditFormJ}
                  user={props.user}
                  getJournals={props.getJournals}
                />
              </Popup>
            </div>
          </div>
        </div>
      ) : (
        <div>
          <h1> You Need To create a Journal</h1>
        </div>
      )}
    </div>
  );
}
