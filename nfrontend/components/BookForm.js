import { useState } from "react";
import { FaTimes } from "react-icons/fa";
import createbook from "../public/Images//createbook.jpg";
import Image from "next/image";

function BookForm(props) {
  const [title, setTitle] = useState("");
  const [author, setAuthor] = useState("");

  // handles creating new book
  const handleSubmit = async (e) => {
    e.preventDefault();
    const data = {};
    //need user id due to pydantic model
    data.user_id = props.user.id;
    data.title = title;
    data.author = author;

    const bookUrl = `http://localhost:8000/api/books`;
    const fetchConfig = {
      method: "POST",
      body: JSON.stringify(data),
      headers: {
        Authorization: `Bearer ${props.token}`,
        "Content-Type": "application/json",
      },
    };

    const response = await fetch(bookUrl, fetchConfig);

    if (response.ok) {
      await response.json();
      setTitle("");
      setAuthor("");
      props.getBooks();
      props.setCreateFormT(false)
    } else {
      console.error("Could not add Book, please try again");
    }
  };

  return (
    <>
      <div className="text-center">
        <div className="flex justify-end">
          <button
            className="close-btn p-3 rounded-lg text-white bg-[#402A1E] "
            onClick={() => props.setCreateFormT(false)}
          >
            <FaTimes />
          </button>
        </div>
        <div className="flex justify-center">
          <Image
            className="w-[65%] h-[220px]  shadow-lg border-4 rounded-full"
            src={createbook}
            loading="lazy"
            alt=""
          />
        </div>
        <h1 className="pt-[5%]">Add to your library</h1>
        <div className="max-w-[500px] pt-[90px] m-auto px-2 py-16 w-full">
          <div className="col-span-3 h-auto shadow-xl shadow-gray-900 rounded-xl lg:p-4">
            <div className="py-4">
              <form onSubmit={handleSubmit}>
                <div className="grid lg:grid-cols-1 gap-4 py-2">
                  <div className="form-floating">
                    <p>
                      <label htmlFor="title">Title</label>
                    </p>
                    <input
                      type="text"
                      className="form-control border-2 rounded-lg p-2 w-[250px] border-gray-300"
                      id="title"
                      value={title}
                      onChange={(e) => setTitle(e.target.value)}
                      required
                      placeholder=""
                    />
                  </div>
                  <div className="form-floating">
                    <p>
                      <label htmlFor="author">Author</label>
                    </p>
                    <input
                      type="text"
                      className="form-control border-2 rounded-lg p-2 w-[250px] border-gray-300"
                      id="author"
                      value={author}
                      onChange={(e) => setAuthor(e.target.value)}
                      placeholder=""
                    />
                  </div>
                </div>
                <div className="pt-[5%]">
                  <button className="py-3 px-4 rounded-lg shadow-md text-white bg-[#402A1E]">
                    ADD
                  </button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
export default BookForm;
