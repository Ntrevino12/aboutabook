import { Typography } from "@material-tailwind/react";

export default function Footer() {
  return (
    <footer className="flex w-full flex-row flex-wrap items-center text-[#F2AA52] border-blue-gray-50 justify-center gap-y-6 gap-x-12 py-6 text-center md:justify-between bg-[#402A1E]">
      <Typography className="font-normal">&copy; 2023 AboutABook</Typography>
    </footer>
  );
}
