import bookw from "../public/Images/bookw.jpg";
import cbook from "../public/Images/coffeeandbooks.jpg";
import gbook from "../public/Images/bookandglass.jpg";
import { Carousel, Typography } from "@material-tailwind/react";
import Image from "next/image";

function HomePageCarousel() {
  return (
    <Carousel className="h-96 w-full">
      <div className="relative h-full w-full">
        <Image
          src={cbook}
          loading="lazy"
          alt=""
          className="h-full w-full object-cover"
        />
        <div className="absolute inset-0 grid h-full w-full place-items-center bg-black/75">
          <div className="w-3/4 text-center md:w-2/4">
            <Typography
              variant="h1"
              color="white"
              className="mb-4 text-3xl md:text-4xl lg:text-5xl"
            >
              Victor Hugo
            </Typography>
            <Typography
              variant="lead"
              color="white"
              className="mb-12 opacity-80"
            >
              “To learn to read is to light a fire; every syllable that is
              spelled out is a spark.”
            </Typography>
          </div>
        </div>
      </div>
      <div className="relative h-full w-full">
        <Image
          src={gbook}
          loading="lazy"
          alt=""
          className="h-full w-full object-cover"
        />
        <div className="absolute inset-0 grid h-full w-full place-items-center bg-black/75">
          <div className="w-3/4 text-center md:w-2/4">
            <Typography
              variant="h1"
              color="white"
              className="mb-4 text-3xl md:text-4xl lg:text-5xl"
            >
              Jane Austen
            </Typography>
            <Typography
              variant="lead"
              color="white"
              className="mb-12 opacity-80"
            >
              “If a book is well written, I always find it too short.”
            </Typography>
          </div>
        </div>
      </div>
      <div className="relative h-full w-full">
        <Image
          src={bookw}
          loading="lazy"
          alt=""
          className="h-full w-full object-cover"
        />
        <div className="absolute inset-0 grid h-full w-full place-items-center bg-black/75">
          <div className="w-3/4 text-center md:w-2/4">
            <Typography
              variant="h1"
              color="white"
              className="mb-4 text-3xl md:text-4xl lg:text-5xl"
            >
              John Locke
            </Typography>
            <Typography
              variant="lead"
              color="white"
              className="mb-12 opacity-80"
            >
              “Reading furnishes the mind only with materials of knowledge; it
              is thinking that makes what we read ours.”
            </Typography>
          </div>
        </div>
      </div>
    </Carousel>
  );
}
export default HomePageCarousel;
