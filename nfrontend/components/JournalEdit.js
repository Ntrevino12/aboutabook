import { useState } from "react";
import { FaTimes } from "react-icons/fa";
import editbook from "../public/Images/editbook.jpg";
import Image from "next/image";

function JournalEdit(props) {
  const [id] = useState(props.journal.id);
  const [user_id] = useState(props.journal.user_id);
  const [book_id] = useState(props.journal.book_id);
  const [summary, setSummary] = useState(props.journal.summary);

  //handles submission of all edit forms for journals
  const handleSubmit = async (e) => {
    e.preventDefault();
    const data = {};
    data.id = id;
    data.user_id = user_id;
    data.book_id = book_id;
    data.summary = summary;

    const journalUrl = `http://localhost:8000/api/journals/${id}`;
    const fetchConfig = {
      method: "PUT",
      body: JSON.stringify(data),
      headers: {
        Authorization: `Bearer ${props.token}`,
        "Content-Type": "application/json",
      },
    };

    const response = await fetch(journalUrl, fetchConfig);

    if (response.ok) {
      await response.json();
      props.getJournals();
      props.setEditFormJ(false);
    }
  };

//need to change size of sum box
  return (
    <>
      <div className="text-center">
        <div className="flex justify-end">
          <button
            className="close-btn p-3 text-white bg-[#402A1E] "
            onClick={() => props.setEditFormJ(false)}
          >
            <FaTimes />
          </button>
        </div>
        <div className="flex justify-center">
          <Image
            className="w-[65%] h-[220px] shadow-lg border-4 rounded-full"
            src={editbook}
            loading="lazy"
            alt=""
          />
        </div>
        <h1 className="pt-[5%]">Edit This Journal</h1>
        <div className="max-w-[500px] pt-[90px] m-auto px-2 py-16 w-full">
          <div className="col-span-3 h-auto shadow-xl shadow-gray-900 rounded-xl lg:p-4">
            <div className="py-4">
              <form onSubmit={handleSubmit}>
                <div className="grid lg:grid-cols-1 gap-4 py-2">
                  <div className="form-floating">
                    <p>
                      <label htmlFor="summary">Summary</label>
                    </p>
                    <input
                      type="text"
                      className="form-control border-2 rounded-lg p-2 w-[250px] border-gray-300"
                      id="summary"
                      value={summary}
                      onChange={(e) => setSummary(e.target.value)}
                      required
                      placeholder=""
                    />
                  </div>
                </div>
                <div className="pt-[5%]">
                  <button className=" py-3 px-4 rounded-lg shadow-md text-white bg-[#402A1E]">
                    Change
                  </button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export default JournalEdit;
