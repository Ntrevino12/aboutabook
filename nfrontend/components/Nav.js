"use client";
import React, { useState, useContext } from "react";
import Link from "next/link";
import { useRouter } from "next/navigation";
import useToken, { AuthContext } from "@galvanize-inc/jwtdown-for-react";
import useUser from "./UseUser";

// need remake nav
const Nav = () => {
  // eslint-disable-next-line
  const [navBg, setNavBg] = useState("#402A1E");
  // eslint-disable-next-line
  const [linkColor, setLinkColor] = useState("#F2AA52");
  const { token } = useContext(AuthContext);
  const { logout } = useToken();
  const router = useRouter();
  const user = useUser(token);

  const handleLogout = () => {
    logout();
    router.push("/");
  };

  return (
    <div
      style={{ backgroundColor: `${navBg}` }}
      className={"fixed w-full h-20 z-[100]"}
    >
      <div className="flex justify-between items-center w-full h-full px-2 2xl:px-16">
        <div>
          {token && user ? (
            <ul style={{ color: `${linkColor}` }} className="flex">
              <li className="ml-10 text-md uppercase hover:border-b">
                <Link href="/">Home</Link>
              </li>
              <li className="ml-10 text-md uppercase hover:border-b">
                <Link href="/books">Books</Link>
              </li>
              <li className="ml-10 text-md uppercase hover:border-b">
                <Link href="/">
                  <button className="text-[#F2AA52]" onClick={handleLogout}>
                    Logout
                  </button>
                </Link>
              </li>
            </ul>
          ) : null}
          {token === null || user === null ? (
            <ul style={{ color: `${linkColor}` }} className="flex">
              <li className="ml-10 text-md uppercase hover:border-b">
                <Link href="/Login">Login</Link>
              </li>
              <li className="ml-10 text-md uppercase hover:border-b">
                <Link href="/signup">SignUp</Link>
              </li>
            </ul>
          ) : null}
        </div>
        <div>
          <Link href="/">
            <h2 className="text-[#F2AA52]">AboutABook</h2>
          </Link>
        </div>
      </div>
    </div>
  );
};

export default Nav;
