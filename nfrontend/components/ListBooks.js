'use client'
import Popup from "./Popup";
import BookEdit from "./BookEdit";
import BookMenu from "./BookMenu"

export default function ListBooks(props) {
  //deletes book by book_id
  const handleDelete = async (book_id) => {
    const deleteUrl = `http://localhost:8000/api/books/${book_id}`;
    const config = {
      method: "delete",
      headers: { Authorization: `Bearer ${props.token}` },
    };
    const response = await fetch(deleteUrl, config);

    if (response.ok) {
      await props.getBooks();
    }
  };

  return (
    <div>
      {props.books ? (
        <div>
          <div className="pt-10">
            <ul className="grid gap-[1%] md:grid-cols-2 lg:grid-cols-3 xl:grid-cols-3 2xl:grid-cols-6">
              {props.books.map((item) => {
                return (
                  <li
                    key={item.id}
                    className="max-w-lg rounded overflow-hidden shadow-lg border-4 border-[#402A1E]"
                  >
                    <div className="flex justify-end">
                      <BookMenu
                        handleDelete={handleDelete}
                        item={item}
                        editBook={props.editBook}
                      />
                    </div>
                    <div className="px-4 py-3">
                      <div className="font-bold text-xl mb-2">{item.title}</div>
                      <p className="text-gray-700 text-base">{item.author}</p>
                    </div>
                  </li>
                );
              })}
            </ul>
            <div>
              <Popup trigger={props.editFormB}>
                <BookEdit
                  book={props.book}
                  setEditFormB={props.setEditFormB}
                  token={props.token}
                  user={props.user}
                  getBooks={props.getBooks}
                />
              </Popup>
            </div>
          </div>
        </div>
      ) : (
        <div>
          <h1> You Need To create a book</h1>
        </div>
      )}
    </div>
  );
}
