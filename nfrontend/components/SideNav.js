"use client";
import { useContext, useState } from "react";
import { AiOutlineClose, AiOutlineMenu } from "react-icons/ai";
import Link from "next/link";
import useToken, { AuthContext } from "@galvanize-inc/jwtdown-for-react";
import useUser from "./UseUser";
import { useRouter } from "next/navigation";

export default function SideNav() {
  const [isOpen, setIsOpen] = useState(false);
  const { token } = useContext(AuthContext);
  const user = useUser(token);
  const { logout } = useToken();
  const router = useRouter();

  const handleLogout = () => {
    logout();
    router.push("/");
  };

  return (
    <nav className="">
      <div className={`absolute z-50 ${!isOpen ? "block" : "hidden"}`}>
        <button
          onClick={() => setIsOpen(!isOpen)}
          className="flex items-center ml-1 mt-1 px-3 py-2 rounded text-black-500 border-2 hover:text-black-400"
        >
          <AiOutlineMenu />
        </button>
      </div>
      <div
        className={`flex-col w-1/3 z-50 border-r-8 py-2 h-screen flex-grow absolute min-w-fit bg-white ${
          isOpen ? "block" : "hidden"
        }`}
      >
        <button
          className="border-2 border-black rounded-full ml-1 py-2 px-2 hover:bg-gray-500 drop-shadow-2xl"
          onClick={() => setIsOpen(!isOpen)}
        >
          <AiOutlineClose />
        </button>
        <div className="border-b-black border-b-8">
          <div>
            <Link className="ml-2 text-center text-[#F2AA52]" href="/">
              <h2 className="">AboutABook</h2>
            </Link>
          </div>
        </div>

        <div>
          {user && token ? (
            <ul className="font-medium pt-4 space-y-2">
              <li className="ml-2 text-lg uppercase hover:border hover:font-bold border-transparent">
                <Link href="/books">Books</Link>
              </li>
              <li className="ml-2 text-lg hover:border hover:font-bold border-transparent">
                <Link href="/">
                  <button className="uppercase" onClick={handleLogout}>
                    Logout
                  </button>
                </Link>
              </li>
            </ul>
          ) : (
            <ul className="font-medium pt-4 space-y-2">
              <li className="ml-2 text-md uppercase hover:border hover:font-bold border-transparent">
                <Link href="/Login">Login</Link>
              </li>
              <li className="ml-2 text-md uppercase hover:border hover:font-bold border-transparent">
                <Link href="/signup">SignUp</Link>
              </li>
            </ul>
          )}
        </div>
      </div>
    </nav>
  );
}
