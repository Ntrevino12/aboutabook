'use client'
import {
  Menu,
  MenuHandler,
  MenuList,
  MenuItem,
} from "@material-tailwind/react";
import { FaEdit } from "react-icons/fa";

function JournalMenu(props) {
  return (
    <Menu
      animate={{
        mount: { y: 0 },
        unmount: { y: 25 },
      }}
    >
      <MenuHandler>
        <button className="">
          <FaEdit className="h-7 w-7 hover:text-[#402A1E]" />
        </button>
      </MenuHandler>
      <MenuList className="bg-[#D9B88F] border-[#402A1E]">
        <MenuItem onClick={() => props.editJournalToggle(props.item)}>edit</MenuItem>
        <MenuItem onClick={() => props.handleDelete(props.item.id)}>
          Delete
        </MenuItem>
      </MenuList>
    </Menu>
  );
}
export default JournalMenu;
