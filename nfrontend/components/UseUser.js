"use client";
import { useState, useEffect } from "react";


// Not working properly
const useUser = (token) => {
  const [user, setUser] = useState(null);

  useEffect(() => {
    if (!token) {
      return;
    }

    const getUser = async () => {
      const result = await fetch(`http://localhost:8000/token`, {
        method: "get",
        credentials: "include",
      });
      const { account: user } = await result.json();
      setUser(user);
    };
    getUser();
  }, [token]);
  return user;
};

export default useUser;
