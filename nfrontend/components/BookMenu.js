'use client'
import {
  Menu,
  MenuHandler,
  MenuList,
  MenuItem,
} from "@material-tailwind/react";
import { FaEdit } from "react-icons/fa";
import Link from "next/link";

function BookMenu(props) {
  return (
    <Menu
      animate={{
        mount: { y: 0 },
        unmount: { y: 25 },
      }}
    >
      <MenuHandler>
        <button className="">
          <FaEdit className="h-7 w-7 hover:text-[#402A1E]" />
        </button>
      </MenuHandler>
      <MenuList className="bg-[#D9B88F] border-[#402A1E]">
        <MenuItem onClick={() => props.editBook(props.item)}>edit</MenuItem>
        <MenuItem>
          <Link
            href={`Journals/${props.item.id}`}
            params={{ id: props.item.id }}
          >
            Journals
          </Link>
        </MenuItem>
        <MenuItem onClick={() => props.handleDelete(props.item.id)}>
          Delete
        </MenuItem>
      </MenuList>
    </Menu>
  );
}
export default BookMenu
