/** @type {import('next').NextConfig} */
const nextConfig = {
    reactStrictMode: true,
    webpack: (config) => {
      config.watchOptions = {
        poll: 800, // Check for changes every .8 of a second
        aggregateTimeout: 300, // delay berfore rebuilding
        ignored: ["**/node_modules"],
      };
      return config;
    },
  };

export default nextConfig;
