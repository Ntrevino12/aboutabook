steps = [
    [
        """
        CREATE TABLE journals(
            id SERIAL PRIMARY KEY NOT NULL,
            user_id INT NOT NULL,
            book_id INT NOT NULL,
            summary TEXT
        );
        """,
        """
        DROP TABLE journals
        """,
    ]
]
