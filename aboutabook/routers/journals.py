from fastapi import  APIRouter, Depends, Response
from queries.journals import (
    JournalIn,
    JournalOut,
    JournalQueries,
    JournalAll
)
from typing import List
from authenticator import authenticator

router = APIRouter()


@router.post("/api/books/{book_id}/journals", response_model=JournalOut)
def create_journal(
    book_id:int,
    info:JournalIn,
    repo:JournalQueries = Depends(),
    account_data=Depends(authenticator.get_current_account_data),
):
    user_id = account_data["id"]
    return repo.create(user_id, book_id, info)

@router.get("/api/books/{book_id}/journals", response_model=List[JournalOut])
def get_all_journals_for_book(
    book_id: int,
    repo: JournalQueries = Depends(),
    account_data=Depends(authenticator.get_current_account_data)
):
    user_id = account_data["id"]
    return repo.get_book_journals(repo, user_id, book_id)

@router.get("/api/journals", response_model=List[JournalOut])
def get_all_journals(
    book_id: int,
    repo: JournalQueries = Depends(),
    account_data=Depends(authenticator.get_current_account_data)
):
    user_id = account_data["id"]
    return repo.get_all_journals(repo, user_id, book_id)

@router.get("/api/journals/{journal_id}", response_model=JournalOut)
def get_one_journal(
    journal_id:int,
    repo:JournalQueries = Depends(),
    account_data=Depends(authenticator.get_current_account_data),
):
    user_id=account_data["id"]
    return repo.get_one(user_id, journal_id)

@router.put("/api/journals/{journal_id}", response_model=JournalOut)
def update_journal(
    journal_id: int,
    info: JournalIn,
    repo: JournalQueries = Depends(),
    account_data = Depends(authenticator.get_current_account_data),
):
    user_id=account_data["id"]
    return repo.update_journal_entry(user_id, journal_id, info)

@router.delete("/api/journals/{journal_id}", response_model=bool)
def delete_journal(
    journal_id:int,
    repo: JournalQueries = Depends(),
    account_data = Depends(authenticator.get_current_account_data),
):
    user_id=account_data["id"]
    return repo.delete(journal_id, user_id)
