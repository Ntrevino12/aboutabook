from fastapi import FastAPI
from routers import accounts, books, journals
from authenticator import authenticator
import os
from fastapi.middleware.cors import CORSMiddleware

app = FastAPI()

origins = [
    os.environ.get("CORS_HOST", None),
    "http://localhost:3000",
]

app.include_router(accounts.router)
app.include_router(authenticator.router)
app.include_router(books.router)
app.include_router(journals.router)

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)
