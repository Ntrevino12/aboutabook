from pydantic import BaseModel
from queries.pool import pool
from typing import List, Optional


class BookIn(BaseModel):
    user_id: int
    title: str
    author: Optional[str]

class BookOut(BookIn):
    id: int



class BookQueries:
    def get_all(self, book: BookIn, user_id: int) -> List[BookOut]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        SELECT *
                        FROM books AS b
                        WHERE b.user_id = %s
                        """,
                        [user_id],
                    )
                    books = []
                    for record in db:
                        book = BookOut(
                            id=record[0],
                            user_id=record[1],
                            title=record[2],
                            author=record[3],
                        )
                        books.append(book)
                    return books
        except Exception:
            return{"message": "Couldnt get books"}

    def create(self, book: BookIn, user_id: int) -> BookOut:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result =db.execute(
                        """
                        INSERT INTO books
                            (
                            user_id
                            , title
                            , author
                            )
                        VALUES ( %s, %s, %s)
                        RETURNING id;
                        """,
                        [
                            user_id,
                            book.title,
                            book.author,
                        ],
                    )
                    id = result.fetchone()[0]
                    old_data = book.dict()
                    return BookOut(id=id, **old_data)
        except Exception:
            return {"message": "Couldnt create the book"}

    def get_one(self, user_id: int, book_id: int) -> BookOut:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        SELECT id,
                        user_id,
                        title,
                        author
                        FROM books
                        WHERE id = %s
                        AND user_id = %s;
                        """,
                        [
                            book_id,
                            user_id
                        ],
                    )
                    record = db.fetchone()
                    if record is None:
                        return None
                    return BookOut(
                        id=record[0],
                        user_id=record[1],
                        title=record[2],
                        author=record[3],
                    )
        except Exception:
            return {"message": "Couldnt get one book"}

#### NOT WORKING WILL FIX TMRW #######
    def delete(self, id: int, user_id: int) -> bool:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        DELETE from books
                        WHERE id = %s
                        AND user_id = %s;
                        """,
                        [ id, user_id],
                    )
                    return True
        except Exception:
            return False

    def update(self, user_id: int, book_id: int, book: BookOut) -> BookOut:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        UPDATE books
                            set title = %s
                            , author = %s
                        WHERE id = %s And user_id = %s
                        """,
                        [
                            book.title,
                            book.author,
                            book_id,
                            user_id
                        ]
                    )
                    old_data = book.dict()
                    old_data["id"] = book_id
                    old_data["user_id"] = user_id
                    return BookOut(**old_data)
        except Exception:
            return {"message": "Couldnt update book"}
