from pydantic import BaseModel
from queries.pool import pool

class DuplicateUsersError(ValueError):
    pass

class AccountsIn(BaseModel):
    username: str
    password: str
    email: str


class AccountsOut(BaseModel):
    id: int
    username: str
    email: str


class AccountsOutWithPassword(AccountsOut):
    hashed_password: str


class AccountsQueries:
    def create(
        self, accounts: AccountsIn, hashed_password: str
    ) -> AccountsOutWithPassword:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        INSERT INTO account
                            (username, email, hashed_password)
                        VALUES
                            (%s, %s, %s)
                        RETURNING id;
                        """,
                        [accounts.username, accounts.email, hashed_password],
                    )
                    id = result.fetchone()[0]
                    return AccountsOutWithPassword(
                        id=id,
                        username=accounts.username,
                        email=accounts.email,
                        hashed_password=hashed_password,
                    )
        except Exception:
            return {"message": "Could not create user"}

    def get(self, username: str) -> AccountsOutWithPassword:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT id
                            , username
                            , email
                            , hashed_password
                        FROM account
                        WHERE username = %s
                        """,
                        [username],
                    )
                    record = result.fetchone()
                    if record is None:
                        return None
                    account = AccountsOutWithPassword(
                        id=record[0],
                        username=record[1],
                        email=record[2],
                        hashed_password=record[3],
                    )
                    return account
        except Exception:
            return {"message": "Could not get user"}
